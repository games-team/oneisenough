== About the game ==

Economic development in the land of the balls has rendered communism an
obsolete ideology, but one ball is ready to stand against the cruel corporate
oppressors. Is one ball enough to free the capitalist balls from their
egocentric ideology and send them to labour camps, or is the process doomed to
fail? There's only one way to find out...

The game was developed as a warmup project for pyweek 4 by Olli Etuaho aka. Hectigo.


== Software requirements ==

These are for the source code version. Windows binary should work out of the box.

 * Python 2.4+
 * Pygame 1.7+


== Playing the game ==

Arrow keys move your ball, number one. Esc quits the game. Provoke nearby
capitalist balls to attack you by pressing Z, and you're able to convert
capitalists to follower balls. Follower balls will follow you around, and try
to convert further capitalist balls by colliding with them. You can also send
the followers one by one to the labour camp by pressing C. Converting is a
consuming process, though, so make sure you outnumber the enemy when you
attack. The enemies know when you're low on hp, so regenerate when you can.
Provocation increases your regeneration rate.


== Development achievements ==

 * Rapid prototyping - from an idea to a working alpha build in two days,
   further features added on following two days.
 * Written from scratch in Python
 * Flocking-based gameplay
 * Simple but stylish graphics


== Possible future features ==

 * More player/capitalist -interaction
 * Improved balance
 * Improved graphics and sounds


Copyright Olli Etuaho 2007.


== Version history ==

You're playing the release version alpha 0.4.

Changes from alpha 0.35 to alpha 0.4:

-Added a game over screen with the possibility to restart the game
-Fixed rendering bugs when window temporarily loses focus
-Added window icon

Changes from alpha 0.32 to alpha 0.35:

-Tweaked scoring a bit
-Improved rendering performance slightly
  -Reduced the provocation star effect size
  -Added image alpha channel conversion
  -Only the playing area is re-rendered - this also makes the effects clip
-Added output for evaluating performance
-Adjusted follower behavior: the followers no longer align with each other
-Added different game over messages

Changes from alpha 0.31 to alpha 0.32:

-Fixed run_game.py path issues in Linux (from Pyweek skellington 1.2).
-Fixed height being passed on to draw function as float, should've been int.
-Revised game balance to allow for longer games

For latest releases and more information, visit:

http://www.hectigo.net/puskutraktori/one.html