import pygame
import os

from pygame.locals import *

from locals import *

from object import Object

class Player(Object):

  def __init__(self, screen, position = None):
    image_file = 'ball1.png'
    self.otype = OBJECT_PLAYER
    Object.__init__(self, screen, image_file, position, PLAYER_SPEED, PLAYER_ACC, PLAYER_BOUNCE, 10, PLAYER_HP)
    
    self.provocation = 0
    self.starsize = 0
    self.staralign = 0
    self.gravity = 0
    self.gravitytarget = []

    return

  def render_back(self):
    self.starsize = self.starsize * 0.8 + self.provocation * 0.2
    self.staralign = self.staralign + self.provocation / 16
    if self.provocation:
      effect = pygame.transform.rotozoom(Object.effectimage, self.staralign, float(self.starsize) / 50)

      effectrect = effect.get_rect()
      effectrect.center = (self.x, self.y)
      self.screen.blit(effect, effectrect)
    Object.render_back(self)
    return

  def provocate(self):
    self.provocation = 200
    self.gravity = 0.4
    self.gravitytarget = [OBJECT_DOLLAR]
    return

  def update(self, borders, collideobjects, move_modifier = 1.0):
    Object.update(self, borders, collideobjects, move_modifier)

    if self.provocation > 0:
      self.provocation -= 4
      self.hp += 1
    else:
      self.gravity = 0
      self.gravitytarget = []

    return