import pygame
import os

from pygame.locals import *

from locals import *

from object import Object

class Bank(Object):

  def __init__(self, screen, position = None, spawntotal = 2):
    image_file = 'bank.png'
    self.otype = OBJECT_BANK
    Object.__init__(self, screen, image_file, position, 0, 0, 0, 10, 1)

    self.time = 0
    self.spawn = False
    self.spawned = 0
    self.spawntotal = spawntotal
    self.height = 0

    return
    
  def render_back(self):
    scaledimage = pygame.transform.scale(self.image, (self.rect.width, int(self.height) ))
    self.rect = scaledimage.get_rect()
    self.rect.center = (self.x, self.y)
    self.screen.blit(scaledimage, self.rect)
    return

  def render(self):
    return

  def update(self, borders, collideobjects, move_modifier = 1.0):
    self.time += 1
    self.height = (100 - ((float(self.time) - 30.0)/3)**2)*0.55
    self.spawn = False
    if self.height > 54.3 and self.spawned < self.spawntotal:
      self.spawned += 1
      self.spawn = True
    if self.height < 0:
      self.dead = True
      self.height = 1;
    return