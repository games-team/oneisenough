import pygame
import os

sounds = {}

def play_sound(filename, volume = 1.0):
    snd = None
    if not sounds.has_key(filename):
        try:
            sound_path = os.path.join("sounds", filename)
            snd = sounds[filename] = pygame.mixer.Sound(sound_path)
        except:
            print "Error: Sound file not found."
            return;
    else:
        snd = sounds[filename]

    snd.set_volume(volume)
    snd.play()
