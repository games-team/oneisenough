import pygame
import os

from pygame.locals import *

from locals import *
from util import *

from object import Object

class Follower(Object):

  def __init__(self, screen, parent = None, position = None):
    image_file = 'ball2.png'
    self.otype = OBJECT_FOLLOWER
    Object.__init__(self, screen, image_file, position, FOLLOWER_SPEED, FOLLOWER_ACC, FOLLOWER_BOUNCE)
    self.parent = parent
    self.to_camp = False
    return

  def update(self, borders, collideobjects, move_modifier = 1.0):
    Object.update(self, borders, collideobjects, move_modifier)
    if (self.parent) and not self.to_camp:
      Object.follow(self, collideobjects, [self.parent.get_type()])
      Object.avoid(self, collideobjects, [self.parent.get_type(), self.get_type()])
      #Object.avoid(self, collideobjects, [OBJECT_DOLLAR], 0.8 * (1 - (self.hp / self.max_hp)), 40)
      Object.follow(self, collideobjects, [OBJECT_DOLLAR], 0.8 * (self.hp / self.max_hp), 40)
      Object.align(self, collideobjects, [self.get_type()])
    if self.to_camp:
      Object.follow(self, collideobjects, [OBJECT_CAMP], 0.1)
      Object.avoid(self, collideobjects, [OBJECT_DOLLAR], 0.8 * (1 - (self.hp / self.max_hp)), 40)
    return
