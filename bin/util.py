def normalize(value):
  abvalue = 0
  for v in value:
    abvalue = abvalue + v**2
  abvalue = abvalue**(0.5)
  if abvalue:
    for v in value:
      v = v / abvalue
  return value

def crossed(value1, value2, border):
  if (value1 <= border and value2 > border) or (value1 >= border and value2 < border):
    return True
  else:
    return False

def absvector(value):
  abvalue = 0
  for v in value:
    abvalue = abvalue + v**2
  abvalue = abvalue**(0.5)
  return abvalue

class Sphere:

  def __init__(self, x, y, radius):
    self.x = x
    self.y = y
    self.radius = radius
    return

  def collidesphere(self, sphere2):
    dist = absvector((self.x - sphere2.x, self.y - sphere2.y))
    radiuses = (self.radius + sphere2.radius)
    if  dist < radiuses:
      return True
    else:
      return False
