import pygame
import os
import random
import copy

from pygame.locals import *

from locals import *
from util import *

from sound import *

class Object:

  invincibleimage = None
  effectimage = None

  def __init__(self, screen, image_file, position = None, max_speed = 12, acc_speed = 1, bounce = 0.5, radius = 13, max_hp = DEFAULT_HP):
    if (Object.invincibleimage == None):
      Object.invincibleimage = pygame.image.load(os.path.join(PICTURES_DIR, "ballinvincible.png")).convert_alpha()
      Object.effectimage = pygame.image.load(os.path.join(PICTURES_DIR, "star.png")).convert_alpha()

    self.screen = screen
    self.image = pygame.image.load(os.path.join(PICTURES_DIR, image_file)).convert_alpha()
    self.rect = self.image.get_rect()

    #Tries to evaluate self.otype. If it fails, the otype is not set yet.
    try:
      self.otype
    except:
      self.otype = OBJECT_GENERIC

    #Tries to set position. If it fails, assigns random position.
    try:
      self.x = position[0]
      self.y = position[1]
    except:
      self.x = SCREEN_WIDTH / 2 + 50 + random.randint(-50, 50)
      self.y = SCREEN_HEIGHT / 2 + random.randint(-50, 50)
      if self.otype == OBJECT_DOLLAR:
        self.x = SCREEN_WIDTH / 2 - 50 + random.randint(-30, 30)
        self.y = SCREEN_HEIGHT / 2 + 40 + random.randint(-30, 30)
      if self.otype == OBJECT_FOLLOWER:
        self.x = SCREEN_WIDTH / 2 + 50 + random.randint(-30, 30)
        self.y = SCREEN_HEIGHT / 2 + 50 + random.randint(-30, 30)

    self.dx = 0
    self.dy = 0
    self.max_speed = max_speed
    self.acc_speed = acc_speed
    self.bounce = bounce
    self.radius = radius
    self.change = OBJECT_NOCHANGE

    self.max_hp = max_hp
    self.hp = max_hp

    self.invincible = INITIAL_INVINCIBILITY

    self.frozen = 0

    self.alternate = 0
    
    self.score = 0
    
    self.dead = False

    return

  #Accelerates with acceleration given as tuplet
  #multiplied with the maximum acceleration of the object:
  def acc(self, acceleration):
    self.dx = self.dx + acceleration[0] * self.acc_speed
    self.dy = self.dy + acceleration[1] * self.acc_speed
    self.speed = (self.dx**2 + self.dy**2)**(0.5)
    if self.speed > self.max_speed:
      speed_mod = self.max_speed / self.speed
      self.dx = speed_mod * self.dx
      self.dy = speed_mod * self.dy
    return


  #Decelerates with maximum acceleration, or with a bit less if dec_mod is set:
  def dec(self, dec_mod = 1):
    self.speed = (self.dx**2 + self.dy**2)**(0.5)
    if self.speed < self.acc_speed:
      self.dx = 0
      self.dy = 0
    else:
      speed_mod = (self.speed - dec_mod) / self.speed
      self.dx = speed_mod * self.dx
      self.dy = speed_mod * self.dy
    return

  def update(self, borders, collide_objects = [], move_modifier = 1.0):
    prevx = self.x
    prevy = self.y

    if (not self.frozen) or self.otype == OBJECT_PLAYER:
      self.x = self.x + (self.dx / 1.5) * move_modifier
      self.y = self.y + (self.dy / 1.5) * move_modifier

    sphere = self.get_sphere()

    if (self.x + self.radius > borders.right):
       self.dx = -(abs(self.dx) + 1) * self.bounce
       self.x = self.x + self.dx
    if (self.x - self.radius < borders.left):
       self.dx = (abs(self.dx) + 1) * self.bounce
       self.x = self.x + self.dx
    if (self.y + self.radius > borders.bottom):
       self.dy = -(abs(self.dy) + 1) * self.bounce
       self.y = self.y + self.dy
    if (self.y - self.radius < borders.top):
       self.dy = (abs(self.dy) + 1) * self.bounce
       self.y = self.y + self.dy

    for o in collide_objects:
      borders = o.get_sphere()
      if sphere.collidesphere(borders) and o != self:
        if (o.otype == OBJECT_FOLLOWER or o.otype == OBJECT_PLAYER) and self.otype == OBJECT_DOLLAR and o.invincible == 0 and self.invincible == 0:
          self.take_damage(DAMAGE_COLLIDE)
          o.take_damage(DAMAGE_COLLIDE)
          self.y = self.y - self.dy
          self.x = self.x - self.dx
          o.y = o.y - o.dy
          o.x = o.x - o.dx
          play_sound("woosh.wav")
          self.frozen = FREEZE_COLLIDE
          o.frozen = FREEZE_COLLIDE
          if (self.hp < 0):
            self.change = OBJECT_FOLLOWER
          if (o.hp < 0):
            o.change = OBJECT_DOLLAR
        if self.otype == OBJECT_FOLLOWER and o.otype == OBJECT_CAMP and self.to_camp:
          self.dead = True
          self.score = 100


    if self.hp < self.max_hp:
      self.hp += 0.5

    if self.invincible > 0:
      self.invincible -= 1

    if self.frozen > 0:
      self.frozen -= 1
      if self.otype != OBJECT_PLAYER:
        self.dec()
      else:
        self.dec(0.3)

    self.alternate = self.alternate + 1 - (self.alternate > 2) * 7

    return

  # Tries to avoid close collision
  def avoid(self, objects, objecttypes, multiplier = 1, max_distance = 500):
    for target in objects:
      for otype in objecttypes:
        if target.get_type() == otype:
          (tx, ty) = target.get_coords()
          distance_from_target = ((self.x - tx)**2 + (self.y - ty)**2)**(0.5)

          if (distance_from_target > max_distance):
            continue

          vector_from_target = normalize((self.x - tx, self.y - ty))
          flocking_acc = normalize(vector_from_target)
          flocking_want = 1 / (distance_from_target + 1)

          flocking_acc = (flocking_acc[0] * flocking_want * multiplier, flocking_acc[1] * flocking_want * multiplier)

          Object.acc(self, flocking_acc)

    return

  # Tries to align with other object's speed
  def align(self, objects, objecttypes, multiplier = 1):
    for target in objects:
      for otype in objecttypes:
        if target.get_type() == otype:
          (dtx, dty) = target.get_speed()
          diff_acc = ((dtx - self.dx), (dty - self.dy))

          flocking_acc = normalize(diff_acc)
          flocking_acc = (flocking_acc[0] * multiplier, flocking_acc[1] * multiplier)
          Object.acc(self, flocking_acc)

          Object.dec(self, 0.1)

    return

  # Follows the target
  def follow(self, objects, objecttypes, multiplier = 1, max_distance = 500):
    for target in objects:
      for otype in objecttypes:
        if target.get_type() == otype:
          (tx, ty) = target.get_coords()
          distance_from_target = ((self.x - tx)**2 + (self.y - ty)**2)**(0.5)

          if (distance_from_target > max_distance):
            continue

          vector_to_target = normalize((tx - self.x, ty - self.y))
          flocking_acc = normalize(vector_to_target)

          flocking_want = (distance_from_target)**(0.5) / 80


          flocking_acc = (flocking_acc[0] * flocking_want * multiplier, flocking_acc[1] * flocking_want * multiplier)

          Object.acc(self, flocking_acc)

          Object.dec(self, 0.3 * multiplier)

    return

  def align(self, objects, objecttypes):
    return

  def get_change(self):
    return self.change

  def get_sphere(self):
    return Sphere(self.x, self.y, self.radius)

  def render_back(self):
    hp_rect = copy.copy(self.rect)
    hp_rect.width = int(self.hp * 50 / self.max_hp)
    hp_rect.height = 3
    hp_rect.top -= 10
    hp_rect.centerx = self.x
    color = (160, 0, 0)
    pygame.draw.rect(self.screen, color, hp_rect)
    if self.hp > DAMAGE_COLLIDE:
      hp_rect.left += (DAMAGE_COLLIDE * 50 / self.max_hp)
      hp_rect.width = ((self.hp - DAMAGE_COLLIDE) * 50 / self.max_hp)
      color = (160, 160, 0)
      pygame.draw.rect(self.screen, color, hp_rect)

    if self.frozen:
      effect = pygame.transform.rotozoom(Object.effectimage, self.frozen * 5, float(self.frozen) / 10)

      effectrect = effect.get_rect()
      effectrect.center = (self.x, self.y)
      self.screen.blit(effect, effectrect)


  def render(self):
    self.rect.center = (self.x, self.y)
    self.screen.blit(self.image, self.rect)

    if self.invincible and (self.alternate > 0):
      self.screen.blit(Object.invincibleimage, self.rect)


    return

  def get_coords(self):
    return (self.x, self.y)

  def get_speed(self):
    return absvector((self.dx, self.dy))

  def get_velocity(self):
    return (self.dx, self.dy)

  def get_type(self):
    return self.otype

  def take_damage(self, amount):
    if not self.invincible:
      self.hp = self.hp - amount
    return