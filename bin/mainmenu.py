#! /usr/bin/env python

import pygame
import time
import os
from pygame.locals import *

from locals import *
import game

smallfont = pygame.font.Font(os.path.join("data","Vera.ttf"), 22)

def render_score(screen, score):
  image = smallfont.render(str(score), 1, (0,0,0))
  rect = image.get_rect()
  rect.centerx = SCREEN_WIDTH / 2 - 1
  rect.top = SCREEN_HEIGHT / 2 + 42
  screen.blit(image, rect)
  rect.centerx = SCREEN_WIDTH / 2 + 2
  screen.blit(image, rect)
  return

def run():
  pygame.init()
  screen = pygame.display.set_mode((SCREEN_WIDTH,SCREEN_HEIGHT))
  pygame.display.set_caption("One is enough.")
  staricon = pygame.image.load(os.path.join(PICTURES_DIR, 'staricon.png'))
  pygame.display.set_icon(staricon)

  clock = pygame.time.Clock()

  exit = False
  gameoverdialog = pygame.image.load(os.path.join(PICTURES_DIR, 'gameover.png')).convert_alpha()
  gameoverrect = gameoverdialog.get_rect()
  gameoverrect.centerx = SCREEN_WIDTH / 2
  gameoverrect.centery = SCREEN_HEIGHT / 2 + 50
  while not exit:
    score = game.run(screen)
    replay = False
    screen.blit(gameoverdialog, gameoverrect)
    render_score(screen, score)
    pygame.display.flip()
    while not replay and not exit:
      for event in pygame.event.get():
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
          exit = True

        if (event.type == KEYDOWN and (event.key == K_SPACE or event.key == K_RETURN or event.key == K_c)):
          replay = True

      clock.tick(FPS)

if __name__ == '__main__':
  run()