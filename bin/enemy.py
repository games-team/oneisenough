import pygame
import os
import random

from pygame.locals import *

from locals import *
from util import *

from object import Object

class Enemy(Object):

  def __init__(self, screen, position = None):
    image_file = 'balldollar.png'
    self.otype = OBJECT_DOLLAR
    Object.__init__(self, screen, image_file, position, ENEMY_SPEED, ENEMY_ACC, ENEMY_BOUNCE)
    return

  def update(self, borders, collideobjects, move_modifier = 1.0):
    Object.update(self, borders, collideobjects, move_modifier)
    self.acc((random.randint(-1,1),random.randint(-1,1)))
    Object.align(self, collideobjects, [self.get_type()])
    Object.follow(self, collideobjects, [self.get_type()], 0.1, 30)
    Object.avoid(self, collideobjects, [self.get_type()], 0.3, 20)
    for co in collideobjects:
      if co.get_type() == OBJECT_PLAYER:
        if co.gravity > 0:
          for gtarget in co.gravitytarget:
            if gtarget == self.otype:
              Object.follow(self, [co], [OBJECT_PLAYER], co.gravity, co.provocation)
        else:
          Object.follow(self, [co], [OBJECT_PLAYER], 0.09 * (2 - co.hp / PLAYER_HP) , PLAYER_HP + 60 - co.hp)


    self.dec(0.3)
    return