import pygame
import os

from pygame.locals import *

from locals import *

class Background:

  def __init__(self, screen):
    self.screen = screen
    self.image = pygame.image.load(os.path.join(PICTURES_DIR, BACKGROUND_IMAGE))
    self.rect = self.image.get_rect()
    return

  def update(self):
    return

  def render(self):
    self.screen.blit(self.image, self.rect)
    return

