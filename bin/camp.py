import pygame
import os

from pygame.locals import *

from locals import *

from object import Object

class Camp(Object):

  def __init__(self, screen, position = None):
    image_file = 'camp.png'
    self.otype = OBJECT_CAMP
    Object.__init__(self, screen, image_file, position, 0, 0, 0, 13, 1)

    self.time = 0
    self.got_target = False
    self.height = 0

    return

  def render_back(self):
    scaledimage = pygame.transform.scale(self.image, (self.rect.width, int(self.height) ))
    self.rect = scaledimage.get_rect()
    self.rect.center = (self.x, self.y)
    self.screen.blit(scaledimage, self.rect)
    return

  def render(self):
    return

  def update(self, borders, collideobjects, move_modifier = 1.0):
    self.time += 1
    self.height = (100 - ((float(self.time) - 30.0)/3)**2)*0.55
    if self.height > 54.3 and not self.got_target:
      self.time -= 1
    if self.height < 0:
      self.dead = True
      self.height = 1;
    return