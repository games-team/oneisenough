python setup.py py2exe -b 1
move dist ..\..\oneisenough_build_bin
cd ..
cd ..
copy "one is enough\README.txt" oneisenough_build_bin
mkdir oneisenough_build_bin\data\
mkdir oneisenough_build_bin\sounds\
mkdir oneisenough_build_bin\pictures\
copy "one is enough\bin\data" oneisenough_build_bin\data\
copy "one is enough\bin\sounds" oneisenough_build_bin\sounds\
copy "one is enough\bin\pictures" oneisenough_build_bin\pictures\
cd "one is enough"
cd bin
del /Q *.pyc
del /Q build\*
rmdir /S /Q build
