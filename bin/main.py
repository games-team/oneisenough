'''Game main module.

Contains the entry point used by the run_game.py script.
'''

import os

os.chdir(os.path.dirname(__file__))

import mainmenu

def main():
    mainmenu.run()    
    return

