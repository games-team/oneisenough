import pygame
import os
import time

from pygame.locals import *

from locals import *

from background import Background
from player import Player
from follower import Follower
from enemy import Enemy
from bank import Bank
from camp import Camp

pygame.font.init()
smallfont = pygame.font.Font(os.path.join("data","Vera.ttf"), 14)

dismiss_delay = 0

# The function sends one of the objects of the corresponding type to the labour camp.
def dismiss(objects, object_type):
  print "Sending 1 ball to camp"
  lowest_hp = DEFAULT_HP + 1
  i = 0
  for object in objects:
    if object.get_type() == object_type:
      if not object.to_camp:
        if object.hp < lowest_hp:
          lowest_hp = object.hp
          lowest_i = i
    i += 1
  try:
    objects[lowest_i].to_camp = True
    objects[lowest_i].dec(5)
  except:
    return 0
  return DISMISS_INTERVAL

def render_score(screen, score):
  image = smallfont.render("Score: "+ str(score), 1, (200,100,10))
  rect = image.get_rect()
  rect.left = 18
  rect.top = 140
  screen.blit(image, rect)
  return

# Main game loop
def run(screen):

  idletime = 0
  comptime = 0
  t1 = time.clock()


  enemies_left = 0
  friends_left = 0
  total_spawned = 1

  t = 0
  spawndelay = INITIAL_SPAWN_DELAY

  dismiss_delay = 0

  clock = pygame.time.Clock()

  objects = []

  player = Player(screen)

  objects.append(player)
  objects.append(Enemy(screen))

  done = False
  lose = False
  lose_delay = LOSE_DELAY
  score = 0

  playingfield = Rect(10, 137, SCREEN_WIDTH - 20, SCREEN_HEIGHT - 149)

  #Pre-rendering the background:
  original_background = Background(screen)
  original_background.render()

  pygame.display.flip()

  #Setting the background to the playing field - quite hackish, but works
  background = Background(screen)
  background.image = pygame.Surface((playingfield.width, playingfield.height))
  background.rect = playingfield

  while not done:

    #Spawning banks
    t += 1
    if t > spawndelay:
      t = 0
      spawndelay = int((spawndelay + 10) * 0.9)
      if enemies_left < MAX_ENEMIES_ON_SCREEN:
        new_enemies = int((friends_left + 1) / 3)
        if new_enemies < MIN_NEW_ENEMIES:
          new_enemies = MIN_NEW_ENEMIES
        objects.append(Bank(screen, None, new_enemies))
        total_spawned += new_enemies

    #Dismiss delay:
    if dismiss_delay > 0:
      dismiss_delay -= 1

    refresh_bg = False

    #Event handling
    for event in pygame.event.get():
      if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
        done = True
        refresh_bg = True
      if event.type == VIDEOEXPOSE or event.type == VIDEORESIZE:
        refresh_bg = True

    keys = pygame.key.get_pressed()

    no_acc = True

    if keys[K_UP]:
      no_acc = False
      player.acc((0,-1))
    if keys[K_DOWN]:
      no_acc = False
      player.acc((0,1))
    if keys[K_LEFT]:
      no_acc = False
      player.acc((-1,0))
    if keys[K_RIGHT]:
      no_acc = False
      player.acc((1,0))
    if keys[K_z]:
      player.provocate()
    if keys[K_c] and dismiss_delay == 0 and friends_left > 0:
      dismiss_delay = dismiss(objects, OBJECT_FOLLOWER)
      camp_exists = False
      for object in objects:
        if object.get_type() == OBJECT_CAMP:
          camp_exists = True
          break
      if not camp_exists:
        objects.append(Camp(screen))

    if no_acc:
      player.dec()

    #Background
    if refresh_bg or lose:
      original_background.render()
    else:
      background.render()

    #Objects
    enemies_left = 0
    friends_left = 0

    for object in objects:
      object.render_back()
      if object.get_type() == OBJECT_DOLLAR:
        enemies_left += 1

      if object.get_type() == OBJECT_FOLLOWER:
        friends_left += 1

    score_mod = 0

    i = 0
    for object in objects:
      # The first parameter is for the playing field, other for object collisions
      if lose:
        object.update(playingfield, objects, float(lose_delay)/LOSE_DELAY)
      else:
        object.update(playingfield, objects)
      change_to = object.get_change()
      coords = object.get_coords()

      if object.get_type() == OBJECT_BANK and object.spawn:
          objects.append(Enemy(screen, coords))

      if lose:
        if object.get_type() == OBJECT_FOLLOWER:
          object.change = OBJECT_DOLLAR
      if change_to:
        if object.get_type() == OBJECT_PLAYER:
          lose = True
          if (score < 5000):
            print "The communist balls lost."
          else:
            if (score < 10000):
              print "The communist balls lost, but didn't go down without a fight. Well done."
            else:
              print "Well, that was quite an upheaval! Not bad, number one, not bad at all."
          print "Now, press ESC."
        del objects[i]
        if change_to == OBJECT_FOLLOWER:
          score_mod += 1
          objects.append(Follower(screen, player, coords))
        if change_to == OBJECT_DOLLAR:
          if not lose:
            score_mod -= 1
          objects.append(Enemy(screen, coords))
      else:
        object.render()
        if object.dead:
          score += object.score
          del objects[i]

      i += 1

    score += score_mod * (100 + total_spawned)

    render_score(screen, score)

    if lose:
      lose_delay -= 1
      if lose_delay == 0:
        done = True

    #Generic
    if refresh_bg:
      pygame.display.flip()
    else:
      pygame.display.update(playingfield)

    #Processing time calculation
    comptime += time.clock() - t1
    t1 = time.clock()
    clock.tick(FPS)
    idletime += time.clock() - t1
    t1 = time.clock()

  print "Idle time: " + str(idletime) + "s, time spent processing: " + str(comptime) + "s"
  print "Average processor time taken: " + str(comptime/(idletime+comptime)*100) + "%"
  return score